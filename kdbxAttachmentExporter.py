#!/usr/bin/env python3

import sys
import os
import time
from pykeepass import PyKeePass
from getpass import getpass

def get_kdbx(path:str):
    passwd = getpass('Enter the kdbx master password: ')
    kp = PyKeePass(sys.argv[1], password=passwd)
    return kp

def get_attachments(kp):
    attachments = kp.attachments
    return attachments

def get_binaries(kp):
    binaries = kp.binaries
    return binaries

def main():
    kdbx = get_kdbx(sys.argv[1])
    binaries = get_binaries(kdbx)
    attachments = get_attachments(kdbx)
    directory:str = 'attachments'

    if not os.path.isdir(directory):
        os.mkdir(directory)

    for idx,binary in enumerate(binaries):
        attachment = kdbx.find_attachments(id=idx, first=True)
        entry = attachment.entry
        fileName:str = entry.title + '_' + attachment.filename
        epoch:str = str(time.time())
        filePath:str = f'./{directory}/{fileName or "attachment"}'

        if os.path.isfile(filePath):
            filePath:str = f'{filePath}_{epoch}'
            print(idx, f'Using alternate path: {filePath}')
        else:
            print(idx, filePath)

        try:
            with open(f'{filePath}', 'wb') as fh:
                fh.write(binary)
                print('file written successfully', '\n')
        except Exception as err:
            print(f'unable to write file: {err}', '\n')

if __name__ == "__main__":
    main()

